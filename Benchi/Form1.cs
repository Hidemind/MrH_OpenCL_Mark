﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;
using System.Diagnostics;
/*using System.Drawing.Drawing2D;
using System.Text;
using Microsoft.Win32;
using System.Collections;
using System.Management;
using System.Runtime.InteropServices;
using System.Reflection;*/



namespace Benchi
{


    public partial class Form1 : Form
    {
        //Liste in der die Threads gespeichert werden (notwendig zum Beenden)
        public List<Thread> Threadlist = new List<Thread>();

        //Ergebnisliste, in der jeder Thread am Ende die Anzahl der Berechnungen (i) ablegt
        public List<decimal> AllThreadCalcCount = new List<decimal>();

        // Flag, ob der Thread gestoppt werden soll.
        bool bstopped;
        bool openCLAvailable;

        //Damit kann man Abwarten, bis ein Event gesetzt wird
        AutoResetEvent _ReadyforNext = new AutoResetEvent(false);

        //Stopwatch sw = new Stopwatch();

        private long currentpoints;

        List<OpenCL.OpenCLDeviceBenchmarks> BenchmarDevices;

        // CPU + GPU

        const int WORK_SIZEX_CPU_TEST = 8100;
        const int WORK_SIZEY_CPU_TEST = 8100;
        const int MIN_CPU_TEST = 0;
        const int MAX_CPU_TEST = 1;

        const int WORK_SIZEX_CACHE_TEST = 625;
        const int WORK_SIZEY_CACHE_TEST = 625;

        const int WORK_SIZE_PI_TEST = 1300000;
        const int TIMES_PI_TEST = 1;

        const int WORK_SIZE_MEM_TEST = 7750;
        const int TIMES_MEM_TEST = 1;

        Random Rand = new Random();

        int timetest;

        public static double Add(ref double location1, double value)
        {
            double newCurrentValue = 0;
            while (true)
            {
                double currentValue = newCurrentValue;
                double newValue = currentValue + value;
                newCurrentValue = Interlocked.CompareExchange(ref location1, newValue, currentValue);
                if (newCurrentValue == currentValue)
                    return newValue;
            }
        }

        public Form1()
        {
            InitializeComponent();
            //InitialisierePerformanceCounter(); 

            //damit kann man von Threads aus Objekte ansteuern, die nicht zum Thread gehören (z.B. schreiben in die GUI). Ist aber keine schöne Practice, besser wären Delegates
            Control.CheckForIllegalCrossThreadCalls = false;
            InitOpenCLCloo();
        }

        private void InitOpenCLCloo()
        {
            try
            {
                BenchmarDevices = new List<OpenCL.OpenCLDeviceBenchmarks>();

                foreach (Cloo.ComputeDevice dev in OpenCL.OpenCLDeviceBenchmarks.GetAllDevices())
                {
                    BenchmarDevices.Add(new OpenCL.OpenCLDeviceBenchmarks(dev, dev.Platform, Console.Out));
                }

                if (BenchmarDevices != null && BenchmarDevices.Count > 0)
                {
                    openCLAvailable = true;
                }
                else
                {
                    // no openCL device present
                    openCLAvailable = false;
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        void SelectComputeDevice(int index)
        {
            if (index < OpenCL.OpenCLDeviceBenchmarks.GetAllDevices().Count)
            {
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        // GPU OPENCL //

        private void MCPUOCL(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareCPUBenchmark(WORK_SIZEX_CPU_TEST, WORK_SIZEY_CPU_TEST);

                while (!bstopped)
                {
                    benchmark.RunCPUBenchmark();
                    Interlocked.Increment(ref currentpoints);
                }

                benchmark.UnprepareCPUBenchmark();
            }
            catch { }
        }

        private void MCPUOCLDP(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareCPUBenchmarkDP(WORK_SIZEX_CPU_TEST, WORK_SIZEY_CPU_TEST);

                while (!bstopped)
                {
                    benchmark.RunCPUBenchmarkDP();
                    Interlocked.Increment(ref currentpoints);
                }

                benchmark.UnprepareCPUBenchmarkDP();
            }
            catch { }
        }

        private void MCACHEOCL(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareCacheBenchmark(WORK_SIZEX_CACHE_TEST, WORK_SIZEY_CACHE_TEST);

                while (!bstopped)
                {
                    benchmark.RunCacheBenchmark();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnprepareCacheBenchmark();
            }
            catch { }
        }

        private void MCACHEOCLDP(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareCacheBenchmarkDP(WORK_SIZEX_CACHE_TEST, WORK_SIZEY_CACHE_TEST);

                while (!bstopped)
                {
                    benchmark.RunCacheBenchmarkDP();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnprepareCacheBenchmarkDP();
            }
            catch { }
        }

        private void MPIOCL(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PreparePIBenchmark(WORK_SIZE_PI_TEST);

                while (!bstopped)
                {
                    benchmark.RunPIBenchmark();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnpreparePIBenchmark();
            }
            catch { }
        }

        private void MPIOCLDP(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PreparePIBenchmarkDP(WORK_SIZE_PI_TEST);

                while (!bstopped)
                {
                    benchmark.RunPIBenchmarkDP();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnpreparePIBenchmarkDP();
            }
            catch { }
        }

        private void MMEMOCL(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareMEMBenchmark(WORK_SIZE_MEM_TEST);

                while (!bstopped)
                {
                    benchmark.RunMEMBenchmark();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnprepareMEMBenchmark();
            }
            catch { }
        }

        private void MMEMOCLDP(object bench)
        {
            try
            {
                OpenCL.OpenCLDeviceBenchmarks benchmark = (OpenCL.OpenCLDeviceBenchmarks)bench;
                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>(() => Stopwatch.StartNew());

                benchmark.PrepareMEMBenchmarkDP(WORK_SIZE_MEM_TEST);

                while (!bstopped)
                {
                    benchmark.RunMEMBenchmarkDP();
                    Interlocked.Increment(ref currentpoints);
                }
                benchmark.UnprepareMEMBenchmarkDP();
            }
            catch { }
        }

        private void Runtest_OCL(ParameterizedThreadStart ThreadName, Label LabelName, bool bSingleThreaded)
        {
            try
            {
                _ReadyforNext.Reset();

                ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>();
                sw.Value = Stopwatch.StartNew();

                // Hilfsvariable für die Anzahl der zu erstellenden Threads
                int i = 0;

                while (!bstopped)
                {
                    if (radioBenchmark.Checked == true)
                    {
                        //Nach 10 Sekunden Stopwatch Stoppen und Stoppedflag auf True setzen 
                        if (sw.Value.ElapsedMilliseconds >= timetest * 1000)
                        {
                            sw.Value.Stop();
                            bstopped = true;
                        }
                        else if (LabelName != null) LabelName.Text = Interlocked.Read(ref currentpoints).ToString();
                    }

                    else if (radioStresstest.Checked == true && checkBox1.Checked == false)
                    {
                        if (sw.Value.ElapsedMilliseconds >= timetest * 1000)
                        {
                            sw.Value.Stop();
                            bstopped = true;
                        }
                        else if (LabelName != null) LabelName.Text = Interlocked.Read(ref currentpoints).ToString();
                    }

                    else if (radioStresstest.Checked == true && checkBox1.Checked == true)
                    {
                        if (LabelName != null) LabelName.Text = Interlocked.Read(ref currentpoints).ToString();
                    }

                    this.Refresh();

                    //soviele Threads erzeugen wie gewünscht. Das machen wir innerhalb von dem Hilfsthread, da es sonst zulange dauert, bis das erste Mal die GUI upgedatet wird (bei mir bis zu 3 Sekunden)
                    while (i < BenchmarDevices.Count)
                    {
                        if (BenchmarDevices[i].Activated)
                        {
                            Thread t = new Thread(ThreadName);

                            //testweise. Wenn man die Priorität zu hoch stellt, kann klarerweise kein anderer Thread was machen, also auch keine GUI Updates
                            t.Priority = ThreadPriority.Highest;

                            //Thread in Threadliste aufnehmen
                            Threadlist.Add(t);
                            t.Start(BenchmarDevices[i]);
                        }
                        i++;
                    }
                }

                //Ab hier ist bstopped true. Threads haben fertig gerechnet und nun kann man die Auswertung machen 

                //Sleep an der Stelle Ist wichtig, sonst wird AllCalcValue evtl ausgewertet, bevor die Threads das Ergebnis reingeschrieben haben.
                System.Threading.Thread.Sleep(750);

                if (LabelName != null) LabelName.Text = Interlocked.Read(ref currentpoints).ToString();

                //auf False setzen, falls man nach einem Durchlauf noch einen 2. machen will
                bstopped = false;

                //Ergebnisliste löschen. Aus dem gleichen Grund. 
                AllThreadCalcCount.Clear();
                _ReadyforNext.Set();

                //Es wurden bisher nur die Schleifen gestoppt. Jetzt killt man alle Threads.
                foreach (Thread t in Threadlist) { if (t.IsAlive) t.Join(); };
            }
            catch
            {
            };
        }
        
        private void RunSelectedTests()
        {
            try
            {
                checkedListBox1.Enabled = false;
                btnStart.Enabled = false;
                goup_option.Enabled = false;

                //auf False setzen, falls man nach einem Durchlauf noch einen 2. machen will
                bstopped = false;

                //Ergebnisliste löschen. Aus dem gleichen Grund.
                Thread t;

                //

                try
                {
                    // Arithmetic //

                    if (bstopped == false)
                    {
                        // Benchmark //

                        if (radioBenchmark.Checked == true)
                        {
                            if (Check_SP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCPUOCL, OCL_CPU, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            if (Check_DP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCPUOCLDP, OCL_CPU, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }
                        }

                        // Stress Test //

                        if (radioStresstest.Checked == true)
                        {
                            if (checkBox1.Checked == false && Check_Arithmetic.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCPUOCL, label23, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            else if (checkBox1.Checked == true && Radio_Arithmetic.Checked == true)
                            {
                                if (checkBox2.Checked == false)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MCPUOCL, label23, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }

                                if (checkBox2.Checked == true)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MCPUOCLDP, label23, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }
                            }
                        }
                    }

                    // Cache //

                    if (bstopped == false)
                    {
                        // Benchmark //

                        if (radioBenchmark.Checked == true)
                        {
                            if (Check_SP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCACHEOCL, OCL_CACHE, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            if (Check_DP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCACHEOCLDP, OCL_CACHE, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }
                        }

                        // Stress Test //

                        if (radioStresstest.Checked == true)
                        {
                            if (checkBox1.Checked == false && Check_Cache.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MCACHEOCL, label24, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            else if (checkBox1.Checked == true && Radio_Cache.Checked == true)
                            {
                                if (checkBox2.Checked == false)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MCACHEOCL, label24, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }

                                if (checkBox2.Checked == true)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MCACHEOCLDP, label24, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }
                            }
                        }
                    }

                    // Pi //

                    if (bstopped == false)
                    {
                        // Benchmark //

                        if (radioBenchmark.Checked == true)
                        {
                            if (Check_SP.Checked == true)
                            {
                                /*AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MPIOCL, OCL_PI, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);*/
                            }

                            if (Check_DP.Checked == true)
                            {
                                /*AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MPIOCLDP, OCL_PI, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);*/
                            }
                        }

                        // Stress Test //

                        if (radioStresstest.Checked == true)
                        {
                            if (checkBox1.Checked == false && Check_Pi.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MPIOCL, label22, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            else if (checkBox1.Checked == true && Radio_Pi.Checked == true)
                            {
                                if (checkBox2.Checked == false)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MPIOCL, label22, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }

                                if (checkBox2.Checked == true)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MPIOCLDP, label22, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }
                            }
                        }
                    }

                    // Memory //

                    if (bstopped == false)
                    {
                        // Benchmark //

                        if (radioBenchmark.Checked == true)
                        {
                            if (Check_SP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MMEMOCL, OCL_MEM, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            if (Check_DP.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MMEMOCLDP, OCL_MEM, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }
                        }

                        // Stress Test //

                        if (radioStresstest.Checked == true)
                        {
                            if (checkBox1.Checked == false && Check_Memory.Checked == true)
                            {
                                AllThreadCalcCount.Clear();
                                currentpoints = 0;
                                t = new Thread(() => Runtest_OCL(MMEMOCL, label17, true));
                                t.Priority = ThreadPriority.Highest;
                                Threadlist.Add(t);
                                t.Start();

                                _ReadyforNext.WaitOne();
                                Thread.Sleep(100);
                            }

                            else if (checkBox1.Checked == true && Radio_Memory.Checked == true)
                            {
                                if (checkBox2.Checked == false)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MMEMOCL, label17, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }

                                if (checkBox2.Checked == true)
                                {
                                    AllThreadCalcCount.Clear();
                                    currentpoints = 0;
                                    t = new Thread(() => Runtest_OCL(MMEMOCLDP, label17, true));
                                    t.Priority = ThreadPriority.Highest;
                                    Threadlist.Add(t);
                                    t.Start();

                                    _ReadyforNext.WaitOne();
                                    Thread.Sleep(100);
                                }
                            }
                        }
                    }
                }
                catch { }

                btnReset.Enabled = true;
                btnSCREENSHOT.Enabled = true;

                if (radioBenchmark.Checked == true)
                {
                    int timetesteing1 = timetest;
                    int timetesteing2 = timetesteing1 / 10;
                    float countcpu1 = Convert.ToInt32(OCL_CPU.Text);
                    float countcache1 = Convert.ToInt32(OCL_CACHE.Text);
                    //float countpi1 = Convert.ToInt32(OCL_PI.Text);
                    float countmem1 = Convert.ToInt32(OCL_MEM.Text);

                    if (Check_SP.Checked == true)
                    {
                        label6.Text = Convert.ToString(Math.Round(Math.Pow(countcpu1 * countcache1 * /*countpi1 **/ countmem1, ((float)1 / 3)), 0));
                        double scale1 = Convert.ToDouble(label6.Text);
                        scale_pro.Text = Convert.ToString(Math.Round((scale1 * 100) / (1290 * timetesteing2), 1) + "% *");
                    }

                    if (Check_DP.Checked == true)
                    {
                        label6.Text = Convert.ToString(Math.Round(Math.Pow(countcpu1 * countcache1 * /*countpi1 **/ countmem1, ((float)1 / 3)), 0));
                        double scale1 = Convert.ToDouble(label6.Text);
                        scale_pro.Text = Convert.ToString(Math.Round((scale1 * 100) / (380 * timetesteing2), 1) + "% *");
                    }
                }

                if (txt_english.Checked == true)
                {
                    btnReset.Text = "RESET";
                }

                if (txt_deutsch.Checked == true)
                {
                    btnReset.Text = "ZURÜCKSETZEN";
                }

                if (txt_chinesisch.Checked == true)
                {
                    btnReset.Text = "重启";
                }

                if (txt_russisch.Checked == true)
                {
                    btnReset.Text = "СБРОС";
                }

                if (txt_ukrainisch.Checked == true)
                {
                    btnReset.Text = "СКИДАННЯ";
                }
            }
            catch
            {
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                checkBox2.Text = "DOUBLE \nPRECISION";

                Radio_Arithmetic.Checked = true;
                groupBox2.Visible = false;
                groupBox3.Visible = true;

                Check_Arithmetic.Visible = true;
                Check_Cache.Visible = true;
                Check_Pi.Visible = true;
                Check_Memory.Visible = true;
                Radio_Arithmetic.Visible = false;
                Radio_Cache.Visible = false;
                Radio_Pi.Visible = false;
                Radio_Memory.Visible = false;

                groupBox18.Visible = false;

                goup_option.Text = "Option";
                goup_benchmark.Text = "Benchmark";
                goup_stress_test.Text = "Stress-Test";
                goup_device.Text = "OpenCL Device";

                ///////////////////

                scale_pro.Text = "% *";
                btnReset.Enabled = false;
                btnSCREENSHOT.Enabled = false;
                checkBox2.Enabled = false;

                var device_items = checkedListBox1.Items;
                int run = 0;
                foreach (OpenCL.OpenCLDeviceBenchmarks benchdev in BenchmarDevices)
                {
                    benchdev.Activated = true;
                    device_items.Add(benchdev);
                    checkedListBox1.SetItemCheckState(run, CheckState.Checked);
                    run++;
                }

                groupBox18.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_english.Checked == true)
                {
                    if (btnReset.Text == "RESET")
                    {
                        _ReadyforNext.Reset();
                        Threadlist.ForEach(t => t.Abort());
                        btnStart.Enabled = true;
                        btnReset.Enabled = false;
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        goup_option.Enabled = true;
                        OCL_CPU.Text = "0";
                        OCL_CACHE.Text = "0";
                        OCL_PI.Text = "0";
                        OCL_MEM.Text = "0";
                        label24.Text = "0";
                        label23.Text = "0";
                        label22.Text = "0";
                        label17.Text = "0";
                        label6.Text = "0";
                        label3.Text = "0";
                        scale_pro.Text = "% *";
                        i = 0;
                        AllThreadCalcCount.Clear();
                        lANGUAGEToolStripMenuItem.Enabled = true;
                    }

                    else if (btnReset.Text == "STOP")
                    {
                        if (radioStresstest.Checked == true && checkBox1.Checked == true)
                        {
                            timer1.Stop();
                        }
                        _ReadyforNext.Set();
                        bstopped = true;
                        btnReset.Text = "RESET";
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        i = 0;
                        Threadlist.ForEach(t => t.Abort());
                    }
                }

                if (txt_deutsch.Checked == true)
                {
                    if (btnReset.Text == "ZURÜCKSETZEN")
                    {
                        _ReadyforNext.Reset();
                        Threadlist.ForEach(t => t.Abort());
                        btnStart.Enabled = true;
                        btnReset.Enabled = false;
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        goup_option.Enabled = true;
                        OCL_CPU.Text = "0";
                        OCL_CACHE.Text = "0";
                        OCL_PI.Text = "0";
                        OCL_MEM.Text = "0";
                        label24.Text = "0";
                        label23.Text = "0";
                        label22.Text = "0";
                        label17.Text = "0";
                        label6.Text = "0";
                        label3.Text = "0";
                        scale_pro.Text = "% *";
                        i = 0;
                        AllThreadCalcCount.Clear();
                        lANGUAGEToolStripMenuItem.Enabled = true;
                    }

                    else if (btnReset.Text == "STOP")
                    {
                        if (radioStresstest.Checked == true && checkBox1.Checked == true)
                        {
                            timer1.Stop();
                        }
                        _ReadyforNext.Set();
                        bstopped = true;
                        btnReset.Text = "ZURÜCKSETZEN";
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        i = 0;
                        Threadlist.ForEach(t => t.Abort());
                    }
                }

                if (txt_chinesisch.Checked == true)
                {
                    if (btnReset.Text == "重启")
                    {
                        _ReadyforNext.Reset();
                        Threadlist.ForEach(t => t.Abort());
                        btnStart.Enabled = true;
                        btnReset.Enabled = false;
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        goup_option.Enabled = true;
                        OCL_CPU.Text = "0";
                        OCL_CACHE.Text = "0";
                        OCL_PI.Text = "0";
                        OCL_MEM.Text = "0";
                        label24.Text = "0";
                        label23.Text = "0";
                        label22.Text = "0";
                        label17.Text = "0";
                        label6.Text = "0";
                        label3.Text = "0";
                        scale_pro.Text = "% *";
                        i = 0;
                        AllThreadCalcCount.Clear();
                        lANGUAGEToolStripMenuItem.Enabled = true;
                    }

                    else if (btnReset.Text == "停止")
                    {
                        if (radioStresstest.Checked == true && checkBox1.Checked == true)
                        {
                            timer1.Stop();
                        }
                        _ReadyforNext.Set();
                        bstopped = true;
                        btnReset.Text = "重启";
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        i = 0;
                        Threadlist.ForEach(t => t.Abort());
                    }
                }

                if (txt_russisch.Checked == true)
                {
                    if (btnReset.Text == "СБРОС")
                    {
                        _ReadyforNext.Reset();
                        Threadlist.ForEach(t => t.Abort());
                        btnStart.Enabled = true;
                        btnReset.Enabled = false;
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        goup_option.Enabled = true;
                        OCL_CPU.Text = "0";
                        OCL_CACHE.Text = "0";
                        OCL_PI.Text = "0";
                        OCL_MEM.Text = "0";
                        label24.Text = "0";
                        label23.Text = "0";
                        label22.Text = "0";
                        label17.Text = "0";
                        label6.Text = "0";
                        label3.Text = "0";
                        scale_pro.Text = "% *";
                        i = 0;
                        AllThreadCalcCount.Clear();
                        lANGUAGEToolStripMenuItem.Enabled = true;
                    }

                    else if (btnReset.Text == "СТОП")
                    {
                        if (radioStresstest.Checked == true && checkBox1.Checked == true)
                        {
                            timer1.Stop();
                        }
                        _ReadyforNext.Set();
                        bstopped = true;
                        btnReset.Text = "СБРОС";
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        i = 0;
                        Threadlist.ForEach(t => t.Abort());
                    }
                }

                if (txt_ukrainisch.Checked == true)
                {
                    if (btnReset.Text == "СКИДАННЯ")
                    {
                        _ReadyforNext.Reset();
                        Threadlist.ForEach(t => t.Abort());
                        btnStart.Enabled = true;
                        btnReset.Enabled = false;
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        goup_option.Enabled = true;
                        OCL_CPU.Text = "0";
                        OCL_CACHE.Text = "0";
                        OCL_PI.Text = "0";
                        OCL_MEM.Text = "0";
                        label24.Text = "0";
                        label23.Text = "0";
                        label22.Text = "0";
                        label17.Text = "0";
                        label6.Text = "0";
                        label3.Text = "0";
                        scale_pro.Text = "% *";
                        i = 0;
                        AllThreadCalcCount.Clear();
                        lANGUAGEToolStripMenuItem.Enabled = true;
                    }

                    else if (btnReset.Text == "СТОП")
                    {
                        if (radioStresstest.Checked == true && checkBox1.Checked == true)
                        {
                            timer1.Stop();
                        }
                        _ReadyforNext.Set();
                        bstopped = true;
                        btnReset.Text = "СКИДАННЯ";
                        btnSCREENSHOT.Enabled = false;
                        checkedListBox1.Enabled = true;
                        i = 0;
                        Threadlist.ForEach(t => t.Abort());
                    }
                }
            }
            catch
            {
            }
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                Bitmap screenshot = new Bitmap(this.Width, this.Height);
                this.DrawToBitmap(screenshot, Rectangle.FromLTRB(0, 0, this.Width, this.Height));
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PNG (*.png*) |*.png";
                save.ShowDialog();
                screenshot.Save(save.FileName, ImageFormat.Png);
            }
            catch
            {
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                bstopped = true;
                _ReadyforNext.Set();
                Thread.Sleep(50);
                Threadlist.ForEach(t => t.Abort());
            }
            catch { }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                if(e.NewValue == CheckState.Checked)
                {
                    BenchmarDevices[checkedListBox1.SelectedIndex].Activated = true;

                }
                else if(e.NewValue == CheckState.Unchecked)
                {
                    BenchmarDevices[checkedListBox1.SelectedIndex].Activated = false;
                }
            }
            catch
            {
            }
        }

        private void btnStart_Click_1(object sender, EventArgs e)
        {
            try
            {
                timetest = 10;
                
                if (radioStresstest.Checked == true)
                {
                    if (radio60.Checked == true)
                    {
                        timetest = 300;
                    }
                    if (radio120.Checked == true)
                    {
                        timetest = 600;
                    }
                    if (radio180.Checked == true)
                    {
                        timetest = 900;
                    }
                    if (radio240.Checked == true)
                    {
                        timetest = 1500;
                    }
                    if (radio360.Checked == true)
                    {
                        timetest = 2700;
                    }
                }

                if (radioStresstest.Checked == true && checkBox1.Checked == true && Radio_Arithmetic.Checked == true)
                {
                    // Arithmetic //
                    label16.Visible = true;
                    label23.Visible = true;
                    // Cache //
                    label18.Visible = false;
                    label24.Visible = false;
                    // Pi //
                    label19.Visible = false;
                    label22.Visible = false;
                    // Memory //
                    label20.Visible = false;
                    label17.Visible = false;
                    i = 0;
                    System.Threading.Thread.Sleep(100);
                    timer1.Start();
                }

                else if (radioStresstest.Checked == true && checkBox1.Checked == true && Radio_Cache.Checked == true)
                {
                    // Arithmetic //
                    label16.Visible = false;
                    label23.Visible = false;
                    // Cache //
                    label18.Visible = true;
                    label24.Visible = true;
                    // Pi //
                    label19.Visible = false;
                    label22.Visible = false;
                    // Memory //
                    label20.Visible = false;
                    label17.Visible = false;
                    i = 0;
                    System.Threading.Thread.Sleep(100);
                    timer1.Start();
                }

                else if (radioStresstest.Checked == true && checkBox1.Checked == true && Radio_Pi.Checked == true)
                {
                    // Arithmetic //
                    label16.Visible = false;
                    label23.Visible = false;
                    // Cache //
                    label18.Visible = false;
                    label24.Visible = false;
                    // Pi //
                    label19.Visible = true;
                    label22.Visible = true;
                    // Memory //
                    label20.Visible = false;
                    label17.Visible = false;
                    i = 0;
                    System.Threading.Thread.Sleep(100);
                    timer1.Start();
                }

                else if (radioStresstest.Checked == true && checkBox1.Checked == true && Radio_Memory.Checked == true)
                {
                    // Arithmetic //
                    label16.Visible = false;
                    label23.Visible = false;
                    // Cache //
                    label18.Visible = false;
                    label24.Visible = false;
                    // Pi //
                    label19.Visible = false;
                    label22.Visible = false;
                    // Memory //
                    label20.Visible = true;
                    label17.Visible = true;
                    i = 0;
                    System.Threading.Thread.Sleep(100);
                    timer1.Start();
                }

                bool OCL_DEVCE_CHECKED = false;

                for (int c = 0; c < checkedListBox1.Items.Count; c++)
                {
                    if (checkedListBox1.GetItemCheckState(c) == CheckState.Checked)
                        OCL_DEVCE_CHECKED = true;
                }

                if (OCL_DEVCE_CHECKED == false)
                {
                    for (int c = 0; c < checkedListBox1.Items.Count; c++)
                    {
                        ((OpenCL.OpenCLDeviceBenchmarks)checkedListBox1.Items[c]).Activated = true;
                        checkedListBox1.SetItemCheckState(c, CheckState.Checked);
                    }

                }

                if (Check_Arithmetic.Checked == false && Check_Cache.Checked == false && Check_Pi.Checked == false && Check_Memory.Checked == false)
                {
                    Check_Arithmetic.Checked = true;
                    Check_Cache.Checked = true;
                    Check_Pi.Checked = true;
                    Check_Memory.Checked = true;
                }

                Thread tests = new Thread(() => RunSelectedTests());
                tests.Start();

                if (txt_english.Checked == true)
                {
                    btnReset.Text = "STOP";
                }

                else if (txt_deutsch.Checked == true)
                {
                    btnReset.Text = "STOP";
                }

                else if (txt_chinesisch.Checked == true)
                {
                    btnReset.Text = "停止";
                }

                else if (txt_russisch.Checked == true)
                {
                    btnReset.Text = "СТОП";
                }

                else if (txt_ukrainisch.Checked == true)
                {
                    btnReset.Text = "СТОП";
                }

                btnReset.Enabled = true;
                lANGUAGEToolStripMenuItem.Enabled = false;
            }
            catch { }
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            if (radioBenchmark.Checked == true)
            {
                groupBox18.Visible = false;
                groupBox3.Visible = true;

                /*goup_option.Size = new System.Drawing.Size(394, 140);
                this.Size = new System.Drawing.Size(439, 630);

                radioBenchmark.Location = new Point(80, 30);
                radioStresstest.Location = new Point(220, 30);
                goup_benchmark.Location = new Point(17, 325);
                goup_device.Location = new Point(17, 37);
                goup_option.Location = new Point(17, 176);
                groupBox18.Location = new Point(15, 52);
                groupBox3.Location = new Point(75, 55);*/

                groupBox18.Enabled = false;
                goup_benchmark.Visible = true;
                goup_stress_test.Visible = false;
            }
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            if (radioStresstest.Checked == true)
            {
                groupBox18.Visible = true;
                groupBox3.Visible = false;

                /*goup_option.Size = new System.Drawing.Size(394, 174);
                this.Size = new System.Drawing.Size(439, 665);

                radioBenchmark.Location = new Point(80, 30);
                radioStresstest.Location = new Point(220, 30);
                goup_device.Location = new Point(17, 37);
                goup_option.Location = new Point(17, 176);
                goup_stress_test.Location = new Point(17, 360);
                groupBox2.Location = new Point(101, 65);
                groupBox18.Location = new Point(15, 52);*/

                groupBox18.Enabled = true;
                goup_benchmark.Visible = false;
                goup_stress_test.Visible = true;
            }
        }

        private void englisch_Click(object sender, EventArgs e)
        {
            try
            {
                txt_english.Checked = true;
                txt_deutsch.Checked = false;
                txt_chinesisch.Checked = false;
                txt_russisch.Checked = false;
                txt_ukrainisch.Checked = false;
                btnSCREENSHOT.Text = "SCREENSHOT";
                lANGUAGEToolStripMenuItem.Text = "LANGUAGE";
                btnReset.Text = "RESET";
                btnStart.Text = "START";
                goup_device.Text = "OpenCL Devices";
                goup_option.Text = "Option";
                radioBenchmark.Text = "BENCHMARK";
                goup_benchmark.Text = "Benchmark";
                radioStresstest.Text = "STRESS TEST";
                goup_stress_test.Text = "Stress-Test";
                radio60.Text = "5MIN";
                radio120.Text = "10MIN";
                radio180.Text = "15MIN";
                radio240.Text = "25MIN";
                radio360.Text = "45MIN";
                label1.Text = "ARITHMETIC";
                label5.Text = "CACHE";
                label9.Text = "PI";
                label2.Text = "MEMORY";
                label8.Text = "SCORE";
                label16.Text = "ARITHMETIC";
                label18.Text = "CACHE";
                label19.Text = "PI";
                label20.Text = "MEMORY";
                Check_Arithmetic.Text = "ARITHMETIC";
                Check_Cache.Text = "CACHE";
                Check_Pi.Text = "PI";
                Check_Memory.Text = "MEMORY";
                Radio_Arithmetic.Text = "ARITHMETIC";
                Radio_Cache.Text = "CACHE";
                Radio_Pi.Text = "PI";
                Radio_Memory.Text = "MEMORY";
                checkBox1.Text = "UNLIMITED";
                groupBox2.Text = "Time - Seconds";
                Check_SP.Text = "SINGLE PRECISION";
                Check_DP.Text = "DOUBLE PRECISION";
                checkBox2.Text = "DOUBLE \nPRECISION";
            }
            catch { };
        }

        private void deutsch_Click(object sender, EventArgs e)
        {
            try
            {
                txt_english.Checked = false;
                txt_deutsch.Checked = true;
                txt_chinesisch.Checked = false;
                txt_russisch.Checked = false;
                txt_ukrainisch.Checked = false;
                btnSCREENSHOT.Text = "SCREENSHOT";
                lANGUAGEToolStripMenuItem.Text = "SPRACHE";
                btnReset.Text = "ZURÜCKSETZEN";
                btnStart.Text = "START";
                goup_device.Text = "OpenCL Geräte";
                goup_option.Text = "Option";
                radioBenchmark.Text = "BENCHMARK";
                goup_benchmark.Text = "Benchmark";
                radioStresstest.Text = "STRESS TEST";
                goup_stress_test.Text = "Stress-Test";
                radio60.Text = "5MIN";
                radio120.Text = "10MIN";
                radio180.Text = "15MIN";
                radio240.Text = "25MIN";
                radio360.Text = "45MIN";
                label1.Text = "ARITHMETIK";
                label5.Text = "CACHE";
                label9.Text = "PI";
                label2.Text = "SPEICHER";
                label8.Text = "ERGEBNIS";
                label16.Text = "ARITHMETIK";
                label18.Text = "CACHE";
                label19.Text = "PI";
                label20.Text = "SPEICHER";
                Check_Arithmetic.Text = "ARITHMETIK";
                Check_Cache.Text = "CACHE";
                Check_Pi.Text = "PI";
                Check_Memory.Text = "SPEICHER";
                Radio_Arithmetic.Text = "ARITHMETIK";
                Radio_Cache.Text = "CACHE";
                Radio_Pi.Text = "PI";
                Radio_Memory.Text = "SPEICHER";
                checkBox1.Text = "UNBEGRENZT";
                groupBox2.Text = "Zeit - Sekunden";
                Check_SP.Text = "EINFACHE GENAUIGKEIT";
                Check_DP.Text = "DOPPELTE GENAUIGKEIT";
                checkBox2.Text = "DOPPELTE \nGENAUIGKEIT";
            }
            catch { };
        }

        private void chinesisch_Click(object sender, EventArgs e)
        {
            try
            {
                txt_english.Checked = false;
                txt_deutsch.Checked = false;
                txt_chinesisch.Checked = true;
                txt_russisch.Checked = false;
                txt_ukrainisch.Checked = false;
                btnSCREENSHOT.Text = "截屏";
                lANGUAGEToolStripMenuItem.Text = "语言";
                btnReset.Text = "重置";
                btnStart.Text = "开始";
                goup_device.Text = "OpenCL 设备";
                goup_option.Text = "选项";
                radioBenchmark.Text = "标准检查程序";
                goup_benchmark.Text = "标准检查程序";
                radioStresstest.Text = "压力测试";
                goup_stress_test.Text = "压力测试";
                radio60.Text = "5分钟";
                radio120.Text = "10分钟";
                radio180.Text = "15分钟";
                radio240.Text = "25分钟";
                radio360.Text = "45分钟";
                label1.Text = "算术";
                label5.Text = "快速缓冲贮存区";
                label9.Text = "圆周率";
                label2.Text = "内存";
                label8.Text = "分数";
                label16.Text = "算术";
                label18.Text = "快速缓冲贮存区";
                label19.Text = "圆周率";
                label20.Text = "内存";
                Check_Arithmetic.Text = "算术";
                Check_Cache.Text = "快速缓冲贮存区";
                Check_Pi.Text = "圆周率";
                Check_Memory.Text = "内存";
                Radio_Arithmetic.Text = "算术";
                Radio_Cache.Text = "快速缓冲贮存区";
                Radio_Pi.Text = "圆周率";
                Radio_Memory.Text = "内存";
                checkBox1.Text = "无限";
                groupBox2.Text = "时间 - 秒";
                Check_SP.Text = "单精度";
                Check_DP.Text = "双精度"; //双精密度
                checkBox2.Text = "双精度";
            }
            catch { };
        }

        private void russisch_Click(object sender, EventArgs e)
        {
            try
            {
                txt_english.Checked = false;
                txt_deutsch.Checked = false;
                txt_chinesisch.Checked = false;
                txt_russisch.Checked = true;
                txt_ukrainisch.Checked = false;
                btnSCREENSHOT.Text = "СКРИНШОТ";
                lANGUAGEToolStripMenuItem.Text = "ЯЗЫК";
                btnReset.Text = "СБРОС";
                btnStart.Text = "СТАРТ";
                goup_device.Text = "устройство OpenCL";
                goup_option.Text = "Опции";
                radioBenchmark.Text = "БЕНЧМАРК";
                goup_benchmark.Text = "Бенчмарк";
                radioStresstest.Text = "СТРЕСС ТЕСТ";
                goup_stress_test.Text = "Стресс Тест";
                radio60.Text = "5мин";
                radio120.Text = "10мин";
                radio180.Text = "15мин";
                radio240.Text = "25мин";
                radio360.Text = "45мин";
                label1.Text = "ВЫЧИСЛЕНИЯ";
                label5.Text = "КЭШ";
                label9.Text = "Пи";
                label2.Text = "ПАМЯТЬ";
                label8.Text = "ОЧКИ";
                label16.Text = "ВЫЧИСЛЕНИЯ";
                label18.Text = "КЭШ";
                label19.Text = "Пи";
                label20.Text = "ПАМЯТЬ";
                Check_Arithmetic.Text = "ВЫЧИСЛЕНИЯ";
                Check_Cache.Text = "КЭШ";
                Check_Pi.Text = "Пи";
                Check_Memory.Text = "ПАМЯТЬ";
                Radio_Arithmetic.Text = "ВЫЧИСЛЕНИЯ";
                Radio_Cache.Text = "КЭШ";
                Radio_Pi.Text = "Пи";
                Radio_Memory.Text = "ПАМЯТЬ";
                checkBox1.Text = "НЕОГРАНИЧЕННЫЙ";
                groupBox2.Text = "Время - Секунды";
                Check_SP.Text = "ОДИНОЧНАЯ ТОЧНОСТЬ";
                Check_DP.Text = "УДВОЕННАЯ ТОЧНОСТЬ";
                checkBox2.Text = "УДВОЕННАЯ \nТОЧНОСТЬ";
            }
            catch { };
        }

        private void ukrainisch_Click(object sender, EventArgs e)
        {
            try
            {
                txt_english.Checked = false;
                txt_deutsch.Checked = false;
                txt_chinesisch.Checked = false;
                txt_russisch.Checked = false;
                txt_ukrainisch.Checked = true;
                btnSCREENSHOT.Text = "СКРІНШОТ";
                lANGUAGEToolStripMenuItem.Text = "МОВА";
                btnReset.Text = "СКИДАННЯ";
                btnStart.Text = "СТАРТ";
                goup_device.Text = "Пристрій OpenCL";
                goup_option.Text = "Опції";
                radioBenchmark.Text = "БЕНЧМАРК";
                goup_benchmark.Text = "Бенчмарк";
                radioStresstest.Text = "СТРЕСС ТЕСТ";
                goup_stress_test.Text = "Стресс Тес";
                radio60.Text = "5хв";
                radio120.Text = "10хв";
                radio180.Text = "15хв";
                radio240.Text = "25хв";
                radio360.Text = "45хв";
                label1.Text = "ВИЧИСЛЕННЯ";
                label5.Text = "КЕШ";
                label9.Text = "Пі";
                label2.Text = "ПАМ'ЯТЬ";
                label8.Text = "ОЧКИ";
                label16.Text = "ВИЧИСЛЕНН";
                label18.Text = "КЕШ";
                label19.Text = "Пі";
                label20.Text = "ПАМ'ЯТЬ";
                Check_Arithmetic.Text = "ВИЧИСЛЕННЯ";
                Check_Cache.Text = "КЕШ";
                Check_Pi.Text = "Пі";
                Check_Memory.Text = "ПАМ'ЯТЬ";
                Radio_Arithmetic.Text = "ВИЧИСЛЕННЯ";
                Radio_Cache.Text = "КЕШ";
                Radio_Pi.Text = "Пі";
                Radio_Memory.Text = "ПАМ'ЯТЬ";
                checkBox1.Text = "НЕОБМЕЖЕНИЙ";
                groupBox2.Text = "Час - Секунди";
                Check_SP.Text = "ОДИНОЧНА ТОЧНІСТЬ";
                Check_DP.Text = "ПОДВОЄНА ТОЧНІСТЬ";
                checkBox2.Text = "ПОДВОЄНА \nТОЧНІСТЬ";
            }
            catch { };
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                radio60.Enabled = false;
                radio120.Enabled = false;
                radio180.Enabled = false;
                radio240.Enabled = false;
                radio360.Enabled = false;
                Check_Arithmetic.Visible = false;
                Check_Cache.Visible = false;
                Check_Pi.Visible = false;
                Check_Memory.Visible = false;
                Radio_Arithmetic.Visible = true;
                Radio_Cache.Visible = true;
                Radio_Pi.Visible = true;
                Radio_Memory.Visible = true;
                groupBox2.Visible = true;
                Radio_Arithmetic.Checked = true;
                checkBox2.Enabled = true;

                groupBox2.Location = new Point(96, 65);

                label16.Location = new Point(72, 136);
                label18.Location = new Point(72, 136);
                label19.Location = new Point(72, 136);
                label20.Location = new Point(72, 136);

                label23.Location = new Point(175, 136);
                label24.Location = new Point(175, 136);
                label22.Location = new Point(175, 136);
                label17.Location = new Point(175, 136);
            }

            else if (checkBox1.Checked == false)
            {
                // Arithmetic //
                label16.Visible = true;
                label23.Visible = true;
                // Cache //
                label18.Visible = true;
                label24.Visible = true;
                // Pi //
                label19.Visible = true;
                label22.Visible = true;
                // Memory //
                label20.Visible = true;
                label17.Visible = true;

                radio60.Enabled = true;
                radio120.Enabled = true;
                radio180.Enabled = true;
                radio240.Enabled = true;
                radio360.Enabled = true;
                Check_Arithmetic.Visible = true;
                Check_Cache.Visible = true;
                Check_Pi.Visible = true;
                Check_Memory.Visible = true;
                Radio_Arithmetic.Visible = false;
                Radio_Cache.Visible = false;
                Radio_Pi.Visible = false;
                Radio_Memory.Visible = false;
                groupBox2.Visible = false;
                checkBox2.Enabled = false;
                checkBox2.Checked = false;

                label16.Location = new Point(29, 47);
                label18.Location = new Point(29, 85);
                label19.Location = new Point(29, 124);
                label20.Location = new Point(29, 163);
                label23.Location = new Point(170, 47);
                label24.Location = new Point(170, 85);
                label22.Location = new Point(170, 124);
                label17.Location = new Point(170, 163);
            }
        }

        double i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            i++;

            if (radioStresstest.Checked == true && checkBox1.Checked == true)
            {
                label3.Text = Convert.ToString((Math.Round(i,1)) / 10);
            }
        }

        private void Radio_Arithmetic_Click(object sender, EventArgs e)
        {
            if(Radio_Arithmetic.Checked == true)
            {
                // Arithmetic //
                label16.Visible = true;
                label23.Visible = true;
                // Cache //
                label18.Visible = false;
                label24.Visible = false;
                // Pi //
                label19.Visible = false;
                label22.Visible = false;
                // Memory //
                label20.Visible = false;
                label17.Visible = false;
            }
        }

        private void Radio_Cache_Click(object sender, EventArgs e)
        {
            if (Radio_Cache.Checked == true)
            {
                // Arithmetic //
                label16.Visible = false;
                label23.Visible = false;
                // Cache //
                label18.Visible = true;
                label24.Visible = true;
                // Pi //
                label19.Visible = false;
                label22.Visible = false;
                // Memory //
                label20.Visible = false;
                label17.Visible = false;
            }
        }

        private void Radio_Pi_Click(object sender, EventArgs e)
        {
            if (Radio_Pi.Checked == true)
            {
                // Arithmetic //
                label16.Visible = false;
                label23.Visible = false;
                // Cache //
                label18.Visible = false;
                label24.Visible = false;
                // Pi //
                label19.Visible = true;
                label22.Visible = true;
                // Memory //
                label20.Visible = false;
                label17.Visible = false;
            }
        }

        private void Radio_Memory_Click(object sender, EventArgs e)
        {
            if (Radio_Memory.Checked == true)
            {
                // Arithmetic //
                label16.Visible = false;
                label23.Visible = false;
                // Cache //
                label18.Visible = false;
                label24.Visible = false;
                // Pi //
                label19.Visible = false;
                label22.Visible = false;
                // Memory //
                label20.Visible = true;
                label17.Visible = true;
            }
        }
    }
}
