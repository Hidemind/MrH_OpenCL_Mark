﻿namespace Benchi
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnStart = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReset = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSCREENSHOT = new System.Windows.Forms.ToolStripMenuItem();
            this.lANGUAGEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_english = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_deutsch = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_russisch = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_ukrainisch = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_chinesisch = new System.Windows.Forms.ToolStripMenuItem();
            this.goup_benchmark = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.scale_pro = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.OCL_MEM = new System.Windows.Forms.Label();
            this.OCL_PI = new System.Windows.Forms.Label();
            this.OCL_CPU = new System.Windows.Forms.Label();
            this.OCL_CACHE = new System.Windows.Forms.Label();
            this.radioStresstest = new System.Windows.Forms.RadioButton();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Radio_Memory = new System.Windows.Forms.RadioButton();
            this.Check_Arithmetic = new System.Windows.Forms.CheckBox();
            this.Radio_Pi = new System.Windows.Forms.RadioButton();
            this.Check_Memory = new System.Windows.Forms.CheckBox();
            this.Radio_Cache = new System.Windows.Forms.RadioButton();
            this.Check_Cache = new System.Windows.Forms.CheckBox();
            this.Radio_Arithmetic = new System.Windows.Forms.RadioButton();
            this.Check_Pi = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.radio360 = new System.Windows.Forms.RadioButton();
            this.radio240 = new System.Windows.Forms.RadioButton();
            this.radio180 = new System.Windows.Forms.RadioButton();
            this.radio120 = new System.Windows.Forms.RadioButton();
            this.radio60 = new System.Windows.Forms.RadioButton();
            this.goup_option = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Check_DP = new System.Windows.Forms.RadioButton();
            this.Check_SP = new System.Windows.Forms.RadioButton();
            this.radioBenchmark = new System.Windows.Forms.RadioButton();
            this.goup_stress_test = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.goup_device = new System.Windows.Forms.GroupBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.goup_benchmark.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.goup_option.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.goup_stress_test.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.goup_device.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label1.Location = new System.Drawing.Point(42, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 26);
            this.label1.TabIndex = 10;
            this.label1.Text = "ARITHMETIC";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label5.Location = new System.Drawing.Point(42, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 26);
            this.label5.TabIndex = 11;
            this.label5.Text = "CACHE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label9.Location = new System.Drawing.Point(42, 111);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 26);
            this.label9.TabIndex = 18;
            this.label9.Text = "PI";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label2.Location = new System.Drawing.Point(42, 149);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 26);
            this.label2.TabIndex = 23;
            this.label2.Text = "MEMORY";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Font = new System.Drawing.Font("Trebuchet MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStart,
            this.btnReset,
            this.btnSCREENSHOT,
            this.lANGUAGEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(426, 32);
            this.menuStrip1.TabIndex = 1238;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(63, 26);
            this.btnStart.Text = "START";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click_1);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(66, 26);
            this.btnReset.Text = "RESET";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSCREENSHOT
            // 
            this.btnSCREENSHOT.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSCREENSHOT.Name = "btnSCREENSHOT";
            this.btnSCREENSHOT.Size = new System.Drawing.Size(115, 26);
            this.btnSCREENSHOT.Text = "SCREENSHOT";
            this.btnSCREENSHOT.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // lANGUAGEToolStripMenuItem
            // 
            this.lANGUAGEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txt_english,
            this.txt_deutsch,
            this.txt_russisch,
            this.txt_ukrainisch,
            this.txt_chinesisch});
            this.lANGUAGEToolStripMenuItem.Font = new System.Drawing.Font("Trebuchet MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lANGUAGEToolStripMenuItem.Name = "lANGUAGEToolStripMenuItem";
            this.lANGUAGEToolStripMenuItem.Size = new System.Drawing.Size(99, 26);
            this.lANGUAGEToolStripMenuItem.Text = "LANGUAGE";
            // 
            // txt_english
            // 
            this.txt_english.AutoSize = false;
            this.txt_english.Checked = true;
            this.txt_english.CheckState = System.Windows.Forms.CheckState.Checked;
            this.txt_english.Name = "txt_english";
            this.txt_english.Size = new System.Drawing.Size(250, 25);
            this.txt_english.Text = "ENGLISH";
            this.txt_english.Click += new System.EventHandler(this.englisch_Click);
            // 
            // txt_deutsch
            // 
            this.txt_deutsch.AutoSize = false;
            this.txt_deutsch.CheckOnClick = true;
            this.txt_deutsch.Name = "txt_deutsch";
            this.txt_deutsch.Size = new System.Drawing.Size(250, 25);
            this.txt_deutsch.Text = "DEUTSCH";
            this.txt_deutsch.Click += new System.EventHandler(this.deutsch_Click);
            // 
            // txt_russisch
            // 
            this.txt_russisch.Name = "txt_russisch";
            this.txt_russisch.Size = new System.Drawing.Size(211, 30);
            this.txt_russisch.Text = "РУССКИЙ";
            this.txt_russisch.Click += new System.EventHandler(this.russisch_Click);
            // 
            // txt_ukrainisch
            // 
            this.txt_ukrainisch.Name = "txt_ukrainisch";
            this.txt_ukrainisch.Size = new System.Drawing.Size(211, 30);
            this.txt_ukrainisch.Text = "УКРАЇНСЬКА";
            this.txt_ukrainisch.Click += new System.EventHandler(this.ukrainisch_Click);
            // 
            // txt_chinesisch
            // 
            this.txt_chinesisch.Name = "txt_chinesisch";
            this.txt_chinesisch.Size = new System.Drawing.Size(211, 30);
            this.txt_chinesisch.Text = "中文";
            this.txt_chinesisch.Click += new System.EventHandler(this.chinesisch_Click);
            // 
            // goup_benchmark
            // 
            this.goup_benchmark.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.goup_benchmark.Controls.Add(this.label10);
            this.goup_benchmark.Controls.Add(this.scale_pro);
            this.goup_benchmark.Controls.Add(this.label6);
            this.goup_benchmark.Controls.Add(this.label8);
            this.goup_benchmark.Controls.Add(this.label1);
            this.goup_benchmark.Controls.Add(this.OCL_MEM);
            this.goup_benchmark.Controls.Add(this.label5);
            this.goup_benchmark.Controls.Add(this.label9);
            this.goup_benchmark.Controls.Add(this.OCL_PI);
            this.goup_benchmark.Controls.Add(this.label2);
            this.goup_benchmark.Controls.Add(this.OCL_CPU);
            this.goup_benchmark.Controls.Add(this.OCL_CACHE);
            this.goup_benchmark.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.goup_benchmark.ForeColor = System.Drawing.Color.Black;
            this.goup_benchmark.Location = new System.Drawing.Point(17, 365);
            this.goup_benchmark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_benchmark.Name = "goup_benchmark";
            this.goup_benchmark.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_benchmark.Size = new System.Drawing.Size(394, 256);
            this.goup_benchmark.TabIndex = 1261;
            this.goup_benchmark.TabStop = false;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label10.Location = new System.Drawing.Point(11, 221);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(371, 26);
            this.label10.TabIndex = 1281;
            this.label10.Text = "* AMD Radeon R7 360 1060/1500 (100%)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scale_pro
            // 
            this.scale_pro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.scale_pro.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.scale_pro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.scale_pro.Location = new System.Drawing.Point(284, 185);
            this.scale_pro.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.scale_pro.Name = "scale_pro";
            this.scale_pro.Size = new System.Drawing.Size(98, 26);
            this.scale_pro.TabIndex = 1280;
            this.scale_pro.Text = "%*";
            this.scale_pro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label6.Location = new System.Drawing.Point(142, 185);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 26);
            this.label6.TabIndex = 1279;
            this.label6.Text = "0";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label8.Location = new System.Drawing.Point(42, 185);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 26);
            this.label8.TabIndex = 1278;
            this.label8.Text = "SCORE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OCL_MEM
            // 
            this.OCL_MEM.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OCL_MEM.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.OCL_MEM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.OCL_MEM.Location = new System.Drawing.Point(142, 149);
            this.OCL_MEM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OCL_MEM.Name = "OCL_MEM";
            this.OCL_MEM.Size = new System.Drawing.Size(132, 26);
            this.OCL_MEM.TabIndex = 1277;
            this.OCL_MEM.Text = "0";
            this.OCL_MEM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OCL_PI
            // 
            this.OCL_PI.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OCL_PI.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.OCL_PI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.OCL_PI.Location = new System.Drawing.Point(142, 111);
            this.OCL_PI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OCL_PI.Name = "OCL_PI";
            this.OCL_PI.Size = new System.Drawing.Size(132, 26);
            this.OCL_PI.TabIndex = 1276;
            this.OCL_PI.Text = "0";
            this.OCL_PI.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OCL_CPU
            // 
            this.OCL_CPU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OCL_CPU.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.OCL_CPU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.OCL_CPU.Location = new System.Drawing.Point(142, 35);
            this.OCL_CPU.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OCL_CPU.Name = "OCL_CPU";
            this.OCL_CPU.Size = new System.Drawing.Size(132, 26);
            this.OCL_CPU.TabIndex = 1274;
            this.OCL_CPU.Text = "0";
            this.OCL_CPU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OCL_CACHE
            // 
            this.OCL_CACHE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OCL_CACHE.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.OCL_CACHE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.OCL_CACHE.Location = new System.Drawing.Point(142, 73);
            this.OCL_CACHE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OCL_CACHE.Name = "OCL_CACHE";
            this.OCL_CACHE.Size = new System.Drawing.Size(132, 26);
            this.OCL_CACHE.TabIndex = 1275;
            this.OCL_CACHE.Text = "0";
            this.OCL_CACHE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioStresstest
            // 
            this.radioStresstest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioStresstest.AutoSize = true;
            this.radioStresstest.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.radioStresstest.Location = new System.Drawing.Point(220, 30);
            this.radioStresstest.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioStresstest.Name = "radioStresstest";
            this.radioStresstest.Size = new System.Drawing.Size(126, 26);
            this.radioStresstest.TabIndex = 1;
            this.radioStresstest.Text = "STRESS TEST";
            this.radioStresstest.UseVisualStyleBackColor = true;
            this.radioStresstest.Click += new System.EventHandler(this.radioButton2_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox18.Controls.Add(this.checkBox2);
            this.groupBox18.Controls.Add(this.groupBox1);
            this.groupBox18.Controls.Add(this.checkBox1);
            this.groupBox18.Controls.Add(this.radio360);
            this.groupBox18.Controls.Add(this.radio240);
            this.groupBox18.Controls.Add(this.radio180);
            this.groupBox18.Controls.Add(this.radio120);
            this.groupBox18.Controls.Add(this.radio60);
            this.groupBox18.Font = new System.Drawing.Font("Trebuchet MS", 6.75F, System.Drawing.FontStyle.Bold);
            this.groupBox18.ForeColor = System.Drawing.Color.Black;
            this.groupBox18.Location = new System.Drawing.Point(18, 52);
            this.groupBox18.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox18.Size = new System.Drawing.Size(358, 112);
            this.groupBox18.TabIndex = 1267;
            this.groupBox18.TabStop = false;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(7, 75);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(139, 22);
            this.checkBox2.TabIndex = 1284;
            this.checkBox2.Text = "Double Precision";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.Controls.Add(this.Radio_Memory);
            this.groupBox1.Controls.Add(this.Check_Arithmetic);
            this.groupBox1.Controls.Add(this.Radio_Pi);
            this.groupBox1.Controls.Add(this.Check_Memory);
            this.groupBox1.Controls.Add(this.Radio_Cache);
            this.groupBox1.Controls.Add(this.Check_Cache);
            this.groupBox1.Controls.Add(this.Radio_Arithmetic);
            this.groupBox1.Controls.Add(this.Check_Pi);
            this.groupBox1.Location = new System.Drawing.Point(123, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 65);
            this.groupBox1.TabIndex = 1267;
            this.groupBox1.TabStop = false;
            // 
            // Radio_Memory
            // 
            this.Radio_Memory.AutoSize = true;
            this.Radio_Memory.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Radio_Memory.Location = new System.Drawing.Point(119, 40);
            this.Radio_Memory.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Radio_Memory.Name = "Radio_Memory";
            this.Radio_Memory.Size = new System.Drawing.Size(86, 22);
            this.Radio_Memory.TabIndex = 1281;
            this.Radio_Memory.Text = "MEMORY";
            this.Radio_Memory.UseVisualStyleBackColor = true;
            this.Radio_Memory.Click += new System.EventHandler(this.Radio_Memory_Click);
            // 
            // Check_Arithmetic
            // 
            this.Check_Arithmetic.AutoSize = true;
            this.Check_Arithmetic.Checked = true;
            this.Check_Arithmetic.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Check_Arithmetic.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_Arithmetic.Location = new System.Drawing.Point(10, 15);
            this.Check_Arithmetic.Name = "Check_Arithmetic";
            this.Check_Arithmetic.Size = new System.Drawing.Size(111, 22);
            this.Check_Arithmetic.TabIndex = 1273;
            this.Check_Arithmetic.Text = "ARITHMETIC";
            this.Check_Arithmetic.UseVisualStyleBackColor = true;
            // 
            // Radio_Pi
            // 
            this.Radio_Pi.AutoSize = true;
            this.Radio_Pi.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Radio_Pi.Location = new System.Drawing.Point(10, 40);
            this.Radio_Pi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Radio_Pi.Name = "Radio_Pi";
            this.Radio_Pi.Size = new System.Drawing.Size(45, 22);
            this.Radio_Pi.TabIndex = 1280;
            this.Radio_Pi.Text = "PI";
            this.Radio_Pi.UseVisualStyleBackColor = true;
            this.Radio_Pi.Click += new System.EventHandler(this.Radio_Pi_Click);
            // 
            // Check_Memory
            // 
            this.Check_Memory.AutoSize = true;
            this.Check_Memory.Checked = true;
            this.Check_Memory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Check_Memory.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_Memory.Location = new System.Drawing.Point(119, 40);
            this.Check_Memory.Name = "Check_Memory";
            this.Check_Memory.Size = new System.Drawing.Size(87, 22);
            this.Check_Memory.TabIndex = 1276;
            this.Check_Memory.Text = "MEMORY";
            this.Check_Memory.UseVisualStyleBackColor = true;
            // 
            // Radio_Cache
            // 
            this.Radio_Cache.AutoSize = true;
            this.Radio_Cache.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Radio_Cache.Location = new System.Drawing.Point(119, 15);
            this.Radio_Cache.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Radio_Cache.Name = "Radio_Cache";
            this.Radio_Cache.Size = new System.Drawing.Size(75, 22);
            this.Radio_Cache.TabIndex = 1279;
            this.Radio_Cache.Text = "CACHE";
            this.Radio_Cache.UseVisualStyleBackColor = true;
            this.Radio_Cache.Click += new System.EventHandler(this.Radio_Cache_Click);
            // 
            // Check_Cache
            // 
            this.Check_Cache.AutoSize = true;
            this.Check_Cache.Checked = true;
            this.Check_Cache.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Check_Cache.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_Cache.Location = new System.Drawing.Point(119, 15);
            this.Check_Cache.Name = "Check_Cache";
            this.Check_Cache.Size = new System.Drawing.Size(76, 22);
            this.Check_Cache.TabIndex = 1274;
            this.Check_Cache.Text = "CACHE";
            this.Check_Cache.UseVisualStyleBackColor = true;
            // 
            // Radio_Arithmetic
            // 
            this.Radio_Arithmetic.AutoSize = true;
            this.Radio_Arithmetic.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Radio_Arithmetic.Location = new System.Drawing.Point(10, 15);
            this.Radio_Arithmetic.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Radio_Arithmetic.Name = "Radio_Arithmetic";
            this.Radio_Arithmetic.Size = new System.Drawing.Size(110, 22);
            this.Radio_Arithmetic.TabIndex = 1278;
            this.Radio_Arithmetic.Text = "ARITHMETIC";
            this.Radio_Arithmetic.UseVisualStyleBackColor = true;
            this.Radio_Arithmetic.Click += new System.EventHandler(this.Radio_Arithmetic_Click);
            // 
            // Check_Pi
            // 
            this.Check_Pi.AutoSize = true;
            this.Check_Pi.Checked = true;
            this.Check_Pi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Check_Pi.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_Pi.Location = new System.Drawing.Point(10, 40);
            this.Check_Pi.Name = "Check_Pi";
            this.Check_Pi.Size = new System.Drawing.Size(46, 22);
            this.Check_Pi.TabIndex = 1275;
            this.Check_Pi.Text = "PI";
            this.Check_Pi.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(7, 57);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(103, 22);
            this.checkBox1.TabIndex = 1277;
            this.checkBox1.Text = "UNLIMITED";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // radio360
            // 
            this.radio360.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radio360.AutoSize = true;
            this.radio360.Checked = true;
            this.radio360.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio360.Location = new System.Drawing.Point(282, 23);
            this.radio360.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radio360.Name = "radio360";
            this.radio360.Size = new System.Drawing.Size(71, 22);
            this.radio360.TabIndex = 1272;
            this.radio360.TabStop = true;
            this.radio360.Text = "45MIN";
            this.radio360.UseVisualStyleBackColor = true;
            // 
            // radio240
            // 
            this.radio240.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radio240.AutoSize = true;
            this.radio240.Checked = true;
            this.radio240.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio240.Location = new System.Drawing.Point(215, 23);
            this.radio240.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radio240.Name = "radio240";
            this.radio240.Size = new System.Drawing.Size(71, 22);
            this.radio240.TabIndex = 1271;
            this.radio240.TabStop = true;
            this.radio240.Text = "25MIN";
            this.radio240.UseVisualStyleBackColor = true;
            // 
            // radio180
            // 
            this.radio180.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radio180.AutoSize = true;
            this.radio180.Checked = true;
            this.radio180.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio180.Location = new System.Drawing.Point(149, 23);
            this.radio180.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radio180.Name = "radio180";
            this.radio180.Size = new System.Drawing.Size(71, 22);
            this.radio180.TabIndex = 1270;
            this.radio180.TabStop = true;
            this.radio180.Text = "15MIN";
            this.radio180.UseVisualStyleBackColor = true;
            // 
            // radio120
            // 
            this.radio120.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radio120.AutoSize = true;
            this.radio120.Checked = true;
            this.radio120.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio120.Location = new System.Drawing.Point(80, 23);
            this.radio120.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radio120.Name = "radio120";
            this.radio120.Size = new System.Drawing.Size(71, 22);
            this.radio120.TabIndex = 1269;
            this.radio120.TabStop = true;
            this.radio120.Text = "10MIN";
            this.radio120.UseVisualStyleBackColor = true;
            // 
            // radio60
            // 
            this.radio60.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radio60.AutoSize = true;
            this.radio60.Checked = true;
            this.radio60.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio60.Location = new System.Drawing.Point(15, 23);
            this.radio60.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radio60.Name = "radio60";
            this.radio60.Size = new System.Drawing.Size(64, 22);
            this.radio60.TabIndex = 1268;
            this.radio60.TabStop = true;
            this.radio60.Text = "5MIN";
            this.radio60.UseVisualStyleBackColor = true;
            // 
            // goup_option
            // 
            this.goup_option.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.goup_option.Controls.Add(this.groupBox3);
            this.goup_option.Controls.Add(this.groupBox18);
            this.goup_option.Controls.Add(this.radioStresstest);
            this.goup_option.Controls.Add(this.radioBenchmark);
            this.goup_option.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.goup_option.ForeColor = System.Drawing.Color.Black;
            this.goup_option.Location = new System.Drawing.Point(17, 176);
            this.goup_option.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_option.Name = "goup_option";
            this.goup_option.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_option.Size = new System.Drawing.Size(394, 179);
            this.goup_option.TabIndex = 1266;
            this.goup_option.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Check_DP);
            this.groupBox3.Controls.Add(this.Check_SP);
            this.groupBox3.Location = new System.Drawing.Point(75, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 78);
            this.groupBox3.TabIndex = 1282;
            this.groupBox3.TabStop = false;
            // 
            // Check_DP
            // 
            this.Check_DP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Check_DP.AutoSize = true;
            this.Check_DP.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_DP.Location = new System.Drawing.Point(40, 46);
            this.Check_DP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Check_DP.Name = "Check_DP";
            this.Check_DP.Size = new System.Drawing.Size(152, 22);
            this.Check_DP.TabIndex = 1281;
            this.Check_DP.Text = "DOUBLE PRECISION";
            this.Check_DP.UseVisualStyleBackColor = true;
            // 
            // Check_SP
            // 
            this.Check_SP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Check_SP.AutoSize = true;
            this.Check_SP.Checked = true;
            this.Check_SP.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check_SP.Location = new System.Drawing.Point(40, 21);
            this.Check_SP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Check_SP.Name = "Check_SP";
            this.Check_SP.Size = new System.Drawing.Size(146, 22);
            this.Check_SP.TabIndex = 1280;
            this.Check_SP.TabStop = true;
            this.Check_SP.Text = "SINGLE PRECISION";
            this.Check_SP.UseVisualStyleBackColor = true;
            // 
            // radioBenchmark
            // 
            this.radioBenchmark.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioBenchmark.AutoSize = true;
            this.radioBenchmark.Checked = true;
            this.radioBenchmark.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.radioBenchmark.Location = new System.Drawing.Point(80, 30);
            this.radioBenchmark.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioBenchmark.Name = "radioBenchmark";
            this.radioBenchmark.Size = new System.Drawing.Size(121, 26);
            this.radioBenchmark.TabIndex = 0;
            this.radioBenchmark.TabStop = true;
            this.radioBenchmark.Text = "BENCHMARK";
            this.radioBenchmark.UseVisualStyleBackColor = true;
            this.radioBenchmark.Click += new System.EventHandler(this.radioButton1_Click);
            // 
            // goup_stress_test
            // 
            this.goup_stress_test.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.goup_stress_test.Controls.Add(this.groupBox2);
            this.goup_stress_test.Controls.Add(this.label16);
            this.goup_stress_test.Controls.Add(this.label17);
            this.goup_stress_test.Controls.Add(this.label18);
            this.goup_stress_test.Controls.Add(this.label19);
            this.goup_stress_test.Controls.Add(this.label20);
            this.goup_stress_test.Controls.Add(this.label22);
            this.goup_stress_test.Controls.Add(this.label23);
            this.goup_stress_test.Controls.Add(this.label24);
            this.goup_stress_test.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.goup_stress_test.ForeColor = System.Drawing.Color.Black;
            this.goup_stress_test.Location = new System.Drawing.Point(17, 365);
            this.goup_stress_test.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_stress_test.Name = "goup_stress_test";
            this.goup_stress_test.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_stress_test.Size = new System.Drawing.Size(394, 256);
            this.goup_stress_test.TabIndex = 1283;
            this.goup_stress_test.TabStop = false;
            this.goup_stress_test.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(7, 201);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 46);
            this.groupBox2.TabIndex = 1279;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Time - Seconds";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label3.Location = new System.Drawing.Point(7, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 26);
            this.label3.TabIndex = 1278;
            this.label3.Text = "0";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label16.Location = new System.Drawing.Point(29, 47);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 26);
            this.label16.TabIndex = 10;
            this.label16.Text = "ARITHMETIC";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label17.Location = new System.Drawing.Point(169, 163);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(131, 26);
            this.label17.TabIndex = 1277;
            this.label17.Text = "0";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label18.Location = new System.Drawing.Point(29, 85);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 26);
            this.label18.TabIndex = 11;
            this.label18.Text = "CACHE";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label19.Location = new System.Drawing.Point(29, 124);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(121, 26);
            this.label19.TabIndex = 18;
            this.label19.Text = "PI";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label20.Location = new System.Drawing.Point(29, 163);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 26);
            this.label20.TabIndex = 23;
            this.label20.Text = "MEMORY";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label22.Location = new System.Drawing.Point(169, 124);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(131, 26);
            this.label22.TabIndex = 1276;
            this.label22.Text = "0";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label23.Location = new System.Drawing.Point(170, 47);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(131, 26);
            this.label23.TabIndex = 1274;
            this.label23.Text = "0";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.label24.Location = new System.Drawing.Point(169, 85);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(131, 26);
            this.label24.TabIndex = 1275;
            this.label24.Text = "0";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.IntegralHeight = false;
            this.checkedListBox1.Location = new System.Drawing.Point(7, 17);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(380, 103);
            this.checkedListBox1.TabIndex = 1266;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // goup_device
            // 
            this.goup_device.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.goup_device.Controls.Add(this.checkedListBox1);
            this.goup_device.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.goup_device.ForeColor = System.Drawing.Color.Black;
            this.goup_device.Location = new System.Drawing.Point(17, 37);
            this.goup_device.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_device.Name = "goup_device";
            this.goup_device.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.goup_device.Size = new System.Drawing.Size(394, 129);
            this.goup_device.TabIndex = 1268;
            this.goup_device.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(426, 634);
            this.Controls.Add(this.goup_device);
            this.Controls.Add(this.goup_stress_test);
            this.Controls.Add(this.goup_option);
            this.Controls.Add(this.goup_benchmark);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "MrH OpenCL-Mark 1.2 64Bit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.goup_benchmark.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.goup_option.ResumeLayout(false);
            this.goup_option.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.goup_stress_test.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.goup_device.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnStart;
        private System.Windows.Forms.ToolStripMenuItem btnReset;
        private System.Windows.Forms.ToolStripMenuItem btnSCREENSHOT;
        private System.Windows.Forms.GroupBox goup_benchmark;
        private System.Windows.Forms.Label OCL_MEM;
        private System.Windows.Forms.Label OCL_PI;
        private System.Windows.Forms.Label OCL_CACHE;
        private System.Windows.Forms.RadioButton radioStresstest;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.RadioButton radio360;
        private System.Windows.Forms.RadioButton radio240;
        private System.Windows.Forms.RadioButton radio180;
        private System.Windows.Forms.RadioButton radio120;
        private System.Windows.Forms.RadioButton radio60;
        private System.Windows.Forms.GroupBox goup_option;
        private System.Windows.Forms.RadioButton radioBenchmark;
        private System.Windows.Forms.Label OCL_CPU;
        private System.Windows.Forms.GroupBox goup_stress_test;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label scale_pro;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripMenuItem lANGUAGEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem txt_english;
        private System.Windows.Forms.ToolStripMenuItem txt_deutsch;
        private System.Windows.Forms.ToolStripMenuItem txt_chinesisch;
        private System.Windows.Forms.ToolStripMenuItem txt_russisch;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.GroupBox goup_device;
        private System.Windows.Forms.ToolStripMenuItem txt_ukrainisch;
        private System.Windows.Forms.CheckBox Check_Memory;
        private System.Windows.Forms.CheckBox Check_Pi;
        private System.Windows.Forms.CheckBox Check_Cache;
        private System.Windows.Forms.CheckBox Check_Arithmetic;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Radio_Memory;
        private System.Windows.Forms.RadioButton Radio_Pi;
        private System.Windows.Forms.RadioButton Radio_Cache;
        private System.Windows.Forms.RadioButton Radio_Arithmetic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton Check_DP;
        private System.Windows.Forms.RadioButton Check_SP;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}

