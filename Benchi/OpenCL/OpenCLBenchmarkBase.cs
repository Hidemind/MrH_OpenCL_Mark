﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloo;
using Cloo.Bindings;
using System.IO;

namespace Benchi.OpenCL
{
    abstract class OpenCLBenchmarkBase : OpenCLBenchmarkInterface
    {
        protected ComputeProgram program;
        protected ComputeContext context;
        protected List<ComputeDevice> device;
        protected ComputeKernel kernel;
        protected ComputeKernel kernel2;
        protected ComputeCommandQueue commands;
        protected string clProgramSource;
        protected TextWriter log;
        protected long[] globalSize;
        protected long[] localSize;

        abstract public void Prepare();
        abstract public void Run();
        abstract public void Unprepare();

        protected void notify(CLProgramHandle programHandle, IntPtr userDataPtr)
        {
            log.WriteLine("Program build notification.");
            byte[] bytes = program.Binaries[0];
            log.WriteLine("Beginning of program binary (compiled for the 1st selected device):");
            log.WriteLine(BitConverter.ToString(bytes, 0, 24) + "...");
        }

        public ComputeCommandQueue GetCommandQueue(ComputeContext context, ComputeDevice device, ComputeCommandQueueFlags properties)
        {
            if (commands == null)
            {
                return new ComputeCommandQueue(context, device, properties);
            }
            else
            {
                return commands;
            }
        }

        protected int IntPow(int x, uint pow)
        {
            int ret = 1;
            while (pow != 0)
            {
                if ((pow & 1) == 1)
                    ret *= x;
                x *= x;
                pow >>= 1;
            }
            return ret;
        }
    }
}
