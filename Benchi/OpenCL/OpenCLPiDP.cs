﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLPiDP : OpenCLBenchmarkBase
    {
        ComputeBuffer<double> Data_GPU;
        double workSize;
        List<long> Reduction_LocalSizes;
        List<long> Reduction_GlobalSizes;
        double limit = 0;

        int Reduction_Iter = 0;

        public OpenCLPiDP(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupsize, long[] workgroupsize_reduction, long workSize, TextWriter log)
        {
            this.workSize = workSize;
            globalSize = new long[1];
            localSize = new long[1];
            this.context = Context;
            this.log = log;
            device = new List<ComputeDevice>();
            device.Add(Device);

            globalSize[0] = workSize;
            localSize[0] = workgroupsize[0];
            limit = workSize;

            clProgramSource = @"
           __kernel void ParallelPI
                    (
                        __global double * Data,
                        __private double limit
                    )
                    {
                        int gid = get_global_id(0);
                        double k_double =  gid;
                        Data[gid] = 0;
                        if(gid < limit)
                        {
                            double pre = (1.0/(pown(16.0, gid)));
                            double next= (4.0/(8*(k_double) + 1) - 2.0f/(8*(k_double) + 4) - 1.0/(8*(k_double) + 5)  -1.0/(8*(k_double) + 6));
                            Data[gid] = pre*next;
                        }
                    }
            
            //Tree-Wise Reduction
            __kernel void PiReduction
                    (
                        __global double * Data,
                        __local double * workspace,
                        __private double limit
                    )
                    {
                        int gid = get_global_id(0);
                        int lid = get_local_id(0);
                        int groupid = get_group_id(0);
                        double test_val = 0;
                        barrier(CLK_LOCAL_MEM_FENCE);
                        if(gid < limit)//fill local memory
                        {
                            test_val = workspace[lid] = Data[gid]; 
                        }
                        else
                        {
                            workspace[lid] = 0; 
                        }
                        barrier(CLK_LOCAL_MEM_FENCE);

                        for(int offset = get_local_size(0) / 2; offset > 0; offset >>= 1) 
                        {
                            if (lid < offset) 
                            {
                                workspace[lid] = workspace[lid] + workspace[lid+offset];
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                        }

                        if(lid==0)
                        {
                            test_val = Data[groupid] =  workspace[lid];
                        }
                    }";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);
            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("ParallelPI");
                kernel2 = program.CreateKernel("PiReduction");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }

            commands = Commands;

            globalSize[0] = (long)((double)localSize[0] * Math.Ceiling((double)(globalSize[0] / (double)localSize[0])));

            Data_GPU = new ComputeBuffer<double>(context, ComputeMemoryFlags.None, globalSize[0]);

            kernel.SetMemoryArgument(0, Data_GPU);
            kernel.SetValueArgument<double>(1, this.limit);

            kernel2.SetMemoryArgument(0, Data_GPU);
            kernel2.SetValueArgument<double>(2, this.limit);

            Reduction_GlobalSizes = new List<long>();
            Reduction_LocalSizes = new List<long>();
            //Calc Iterations
            long LS_Red_Temp = workgroupsize_reduction[0];
            long GS_Red_Temp = (long)((double)LS_Red_Temp * Math.Ceiling((double)(globalSize[0] / (double)LS_Red_Temp)));


            Reduction_GlobalSizes.Add(GS_Red_Temp);
            Reduction_LocalSizes.Add(workgroupsize_reduction[0]);

            Reduction_Iter++;
            GS_Red_Temp /= LS_Red_Temp;
            for (; GS_Red_Temp > 1; Reduction_GlobalSizes.Add(GS_Red_Temp), Reduction_LocalSizes.Add(LS_Red_Temp), Reduction_Iter++, GS_Red_Temp /= LS_Red_Temp)
            {

                if (GS_Red_Temp > workgroupsize_reduction[0])
                {
                    LS_Red_Temp = workgroupsize_reduction[0];
                }
                else
                {
                    GS_Red_Temp = (long)Math.Pow(2.0f, (double)Math.Ceiling(((double)Math.Log((double)GS_Red_Temp) / Math.Log(2.0f))));
                    LS_Red_Temp = GS_Red_Temp;
                }

                GS_Red_Temp = (long)((double)LS_Red_Temp * Math.Ceiling((double)(GS_Red_Temp / (double)LS_Red_Temp)));
            }
        }

        public override void Prepare()
        {
            Data_GPU = new ComputeBuffer<double>(context, ComputeMemoryFlags.None, globalSize[0]);

            kernel.SetMemoryArgument(0, Data_GPU);
            kernel.SetValueArgument<double>(1, this.limit);

            kernel2.SetMemoryArgument(0, Data_GPU);
            kernel2.SetValueArgument<double>(2, this.limit);

        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);

            for (int cnt = 0; cnt < Reduction_Iter; cnt++)
            {
                kernel2.SetLocalArgument(1, sizeof(double) * Reduction_LocalSizes[cnt]);
                commands.Execute(kernel2, null, new long[] { Reduction_GlobalSizes[cnt] }, new long[] { Reduction_LocalSizes[cnt] }, null);
            }

            commands.Finish();
        }

        public override void Unprepare()
        {
            Data_GPU.Dispose();
        }
    }
}