﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cloo;

namespace Benchi
{
    class OpenCLManager
    {

        List<List<ComputeDevice>> Devices;

        List<ComputePlatform> Platforms;

        List<ComputeContext> Context;

        public OpenCLManager()
        {
            Devices = new List<List<ComputeDevice>>();
            Platforms = new List<ComputePlatform>();
            Context = new List<ComputeContext>();

            foreach (var p in ComputePlatform.Platforms)
            {
                Platforms.Add(p);

                Devices.Add(new List<ComputeDevice>());

                foreach (var d in p.Devices)
                {
                    //if (d.Type == ComputeDeviceTypes.Gpu || d.Type == ComputeDeviceTypes.Accelerator)
                    Devices[Devices.Count - 1].Add(d);
                }

                if (Devices[Devices.Count - 1].Count > 0)
                {
                    Context.Add(new ComputeContext(Devices[Devices.Count - 1], new ComputeContextPropertyList(Platforms[Platforms.Count - 1]), null, IntPtr.Zero));
                }
                else
                {
                    Devices.RemoveAt(Devices.Count - 1);
                    Platforms.RemoveAt(Platforms.Count - 1);
                }
            }

            //var properties = new ComputeContextPropertyList(;

            //Context = new ComputeContext(Devices, properties, null, IntPtr.Zero);
        }

        public List<ComputeContext> GetComputeContextList()
        {
            return Context;
        }

        public List<List<ComputeDevice>> GetDeviceLists()
        {
            return Devices;
        }

        public List<ComputePlatform> GetPlatforms()
        {
            return Platforms;
        }

    }
}
