﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLArithmeticDP : OpenCLBenchmarkBase
    {
        double[] parameter;
        ComputeBuffer<double> result;

        public OpenCLArithmeticDP(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupsize, long workSizeX, long workSizeY, TextWriter log, double[] pars)
        {
            this.log = log;
            context = Context;
            device = new List<ComputeDevice>();
            device.Add(Device);
            parameter = new double[pars.Length];
            parameter[0] = pars[0];
            parameter[1] = pars[1];
            parameter[2] = pars[2];
            parameter[3] = pars[3];
            clProgramSource = @"__kernel void
                floatCPUBench(
                            __global double * res,
                            __private double par0,
                            __private double par1,
                            __private double par2,
                            __private double par3
                            )
                {   
                double IntVal = 2;
                double FloatVal = 2.0f;        

                IntVal =  (((IntVal-par0)*par1)+par2)/par3;
                FloatVal =  (((FloatVal-par0)*par1)+par2)/par3;
                FloatVal = exp(log(FloatVal));
                res[get_global_id(0)+get_global_size(0)*get_global_id(1)] =  FloatVal - IntVal;

                }";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);

            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("floatCPUBench");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }


            commands = Commands;

            globalSize = new long[2];
            localSize = new long[2];

            //Workgroup-Size
            globalSize[0] = workSizeX;
            globalSize[1] = workSizeY;

            localSize[0] = workgroupsize[0];
            localSize[1] = workgroupsize[1];

            globalSize[0] = (long)((double)localSize[0] * Math.Ceiling((double)(globalSize[0] / (double)localSize[0])));
            globalSize[1] = (long)((double)localSize[1] * Math.Ceiling((double)(globalSize[1] / (double)localSize[1])));
        }

        public override void Prepare()
        {
            //Reset this Benchmark for re-run
            result = new ComputeBuffer<double>(this.context, ComputeMemoryFlags.None, globalSize[0] * globalSize[1]);

            int par_cnt = 0;
            kernel.SetMemoryArgument(par_cnt++, result);
            kernel.SetValueArgument<double>(par_cnt++, this.parameter[0]);
            kernel.SetValueArgument<double>(par_cnt++, this.parameter[1]);
            kernel.SetValueArgument<double>(par_cnt++, this.parameter[2]);
            kernel.SetValueArgument<double>(par_cnt++, this.parameter[3]);
        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);
            commands.Finish();
        }
        
        public override void Unprepare()
        {
            result.Dispose();
        }
    }
}
