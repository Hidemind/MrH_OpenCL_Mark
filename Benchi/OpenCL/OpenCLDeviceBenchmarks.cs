﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Cloo;

namespace Benchi.OpenCL
{
    class OpenCLDeviceBenchmarks
    {
        string Name;

        public Boolean Activated;

        OpenCLArithmetic CPUBenchmark;
        OpenCLArithmeticDP CPUBenchmarkDP;
        OpenCLCache CacheBenchmark;
        OpenCLCacheDP CacheBenchmarkDP;
        OpenCLPi PIBenchmark;
        OpenCLPiDP PIBenchmarkDP;
        OpenCLMem MemBenchmark;
        OpenCLMemDP MemBenchmarkDP;

        ComputeContext Context;
        ComputeCommandQueue Commands;

        List<ComputeDevice> Device = new List <ComputeDevice>();

        TextWriter log;

        //VendorIDs
        long AMD = 0x1002;
        long NVidia = 0x10DE;
        long Intel = 0x8086;

        //AMD
        long[] _workGroupSizeArith_AMD = { 256, 1, 1 };
        long[] _workGroupSizeCache_AMD = { 16, 16, 1 };
        long[] _workGroupSizePI_AMD = { 16, 16, 1 };
        long[] _workGroupSizePI_Reduction_AMD = { 256, 1, 1 };
        long[] _workGroupSizeMEM_AMD = { 256, 1, 1 };
        //NV
        long[] _workGroupSizeArith_NV = { 512, 1, 1 };
        long[] _workGroupSizeCache_NV = { 32, 16, 1 };
        long[] _workGroupSizePI_NV = { 32, 16, 1 };
        long[] _workGroupSizePI_Reduction_NV = { 512, 1, 1 };
        long[] _workGroupSizeMEM_NV = { 512, 1, 1 };
        //INTEL
        long[] _workGroupSizeArith_INTEL = { 256, 1, 1 };
        long[] _workGroupSizeCache_INTEL = { 16, 16, 1 };
        long[] _workGroupSizePI_INTEL = { 16, 16, 1 };
        long[] _workGroupSizePI_Reduction_INTEL = { 256, 1, 1 };
        long[] _workGroupSizeMEM_INTEL = { 256, 1, 1 };

        //Selected WorkGroupSize
        long[] _workGroupSizeArith_Sel = { 256, 1, 1 };
        long[] _workGroupSizeCache_Sel = { 16, 16, 1 };
        long[] _workGroupSizePI_Sel = { 16, 16, 1 };
        long[] _workGroupSizePI_Reduction_Sel = { 256, 1, 1 };
        long[] _workGroupSizeMEM_Sel = { 256, 1, 1 };

        long _vendorId = 0;
        //long _deviceId = 0;
        string _deviceName = string.Empty;
        string _vendorName = string.Empty;

        public OpenCLDeviceBenchmarks(ComputeDevice device, ComputePlatform platform, TextWriter log)
        {
            Device.Add(device);
            new List<ComputeDevice>(Device);
            Context = new ComputeContext(Device, new ComputeContextPropertyList(device.Platform), null, IntPtr.Zero);
            Name = device.Name;
            this.log = log;
            _vendorId = device.VendorId;
            _vendorName = device.Vendor;
            _deviceName = device.Name;

            if (_vendorId == AMD)
            {
                _workGroupSizeArith_Sel = _workGroupSizeArith_AMD;
                _workGroupSizePI_Sel = _workGroupSizePI_AMD;
                _workGroupSizePI_Reduction_Sel = _workGroupSizePI_Reduction_AMD;
                _workGroupSizeCache_Sel = _workGroupSizeCache_AMD;
                _workGroupSizeMEM_Sel = _workGroupSizeMEM_AMD;

            }
            else if (_vendorId == NVidia)
            {
                _workGroupSizeArith_Sel = _workGroupSizeArith_NV;
                _workGroupSizePI_Sel = _workGroupSizePI_NV;
                _workGroupSizePI_Reduction_Sel = _workGroupSizePI_Reduction_NV;
                _workGroupSizeCache_Sel = _workGroupSizeCache_NV;
                _workGroupSizeMEM_Sel = _workGroupSizeMEM_NV;

            }
            else if (_vendorId == Intel)
            {
                _workGroupSizeArith_Sel = _workGroupSizeArith_INTEL;
                _workGroupSizePI_Sel = _workGroupSizePI_INTEL;
                _workGroupSizePI_Reduction_Sel = _workGroupSizePI_Reduction_INTEL;
                _workGroupSizeCache_Sel = _workGroupSizeCache_INTEL;
                _workGroupSizeMEM_Sel = _workGroupSizeMEM_INTEL;
            }
            else
            {
                _workGroupSizeArith_Sel = new long[] { 256, 1, 1 };
                _workGroupSizeCache_Sel = new long[] { 16, 16, 1 };
                _workGroupSizePI_Sel = new long[] { 16, 16, 1 };
                _workGroupSizePI_Reduction_Sel = new long[] { 256, 1, 1 };
                _workGroupSizeMEM_Sel = new long[] { 256, 1, 1 };
            }

            Commands = new ComputeCommandQueue(Context, Device[0], ComputeCommandQueueFlags.None);
            Activated = true;
        }

        public void PrepareCPUBenchmark(long workSizeX, long workSizeY)
        {
            if (CPUBenchmark == null)
                CPUBenchmark = new OpenCLArithmetic(Context, Device[0], Commands, _workGroupSizeArith_Sel, workSizeX, workSizeY, this.log, new float[] { 1, 2, 1, 1 });

            CPUBenchmark.Prepare();
        }

        public void UnprepareCPUBenchmark()
        {
            CPUBenchmark.Unprepare();
        }

        public void PrepareCPUBenchmarkDP(long workSizeX, long workSizeY)
        {
            if (CPUBenchmarkDP == null)
                CPUBenchmarkDP = new OpenCLArithmeticDP(Context, Device[0], Commands, _workGroupSizeArith_Sel, workSizeX, workSizeY, this.log, new double[] { 1, 2, 1, 1 });

            CPUBenchmarkDP.Prepare();
        }

        public void UnprepareCPUBenchmarkDP()
        {
            CPUBenchmarkDP.Unprepare();
        }

        public void PrepareCacheBenchmark(long workSizeX, long workSizeY)
        {
            if (CacheBenchmark == null)
                CacheBenchmark = new OpenCLCache(Context, Device[0], Commands, _workGroupSizeCache_Sel, workSizeX, workSizeY, this.log);

            CacheBenchmark.Prepare();
        }

        public void UnprepareCacheBenchmark()
        {
            CacheBenchmark.Unprepare();
        }

        public void PrepareCacheBenchmarkDP(long workSizeX, long workSizeY)
        {
            if (CacheBenchmarkDP == null)
                CacheBenchmarkDP = new OpenCLCacheDP(Context, Device[0], Commands, _workGroupSizeCache_Sel, workSizeX, workSizeY, this.log);

            CacheBenchmarkDP.Prepare();
        }

        public void UnprepareCacheBenchmarkDP()
        {
            CacheBenchmarkDP.Unprepare();
        }

        public void PreparePIBenchmark(long workSize)
        {
            if (PIBenchmark == null)
                PIBenchmark = new OpenCLPi(Context, Device[0], Commands, _workGroupSizePI_Sel, _workGroupSizePI_Reduction_Sel, workSize, this.log);

            PIBenchmark.Prepare();
        }

        public void UnpreparePIBenchmark()
        {
            PIBenchmark.Unprepare();
        }

        public void PreparePIBenchmarkDP(long workSize)
        {
            if (PIBenchmarkDP == null)
                PIBenchmarkDP = new OpenCLPiDP(Context, Device[0], Commands, _workGroupSizePI_Sel, _workGroupSizePI_Reduction_Sel, workSize, this.log);

            PIBenchmarkDP.Prepare();
        }

        public void UnpreparePIBenchmarkDP()
        {
            PIBenchmarkDP.Unprepare();
        }

        public void PrepareMEMBenchmark(long workSize)
        {
            if (MemBenchmark == null)
                MemBenchmark = new OpenCLMem(Context, Device[0], Commands, _workGroupSizeMEM_Sel, workSize, this.log);

            MemBenchmark.Prepare();
        }

        public void UnprepareMEMBenchmark()
        {
            MemBenchmark.Unprepare();
        }

        public void PrepareMEMBenchmarkDP(long workSize)
        {
            if (MemBenchmarkDP == null)
                MemBenchmarkDP = new OpenCLMemDP(Context, Device[0], Commands, _workGroupSizeMEM_Sel, workSize, this.log);

            MemBenchmarkDP.Prepare();
        }

        public void UnprepareMEMBenchmarkDP()
        {
            MemBenchmarkDP.Unprepare();
        }

        public void RunCPUBenchmark()
        {
            CPUBenchmark.Run();
        }

        public void RunCPUBenchmarkDP()
        {
            CPUBenchmarkDP.Run();
        }

        public void RunCacheBenchmark()
        {
            CacheBenchmark.Run();
        }

        public void RunCacheBenchmarkDP()
        {
            CacheBenchmarkDP.Run();
        }

        public void RunPIBenchmark()
        {
            PIBenchmark.Run();
        }

        public void RunPIBenchmarkDP()
        {
            PIBenchmarkDP.Run();
        }

        public void RunMEMBenchmark()
        {
            MemBenchmark.Run();
        }

        public void RunMEMBenchmarkDP()
        {
            MemBenchmarkDP.Run();
        }

        public override string ToString()
        {
            return (Name +" ("+Device[0].Vendor+")");
        }

        public static List<ComputeDevice> GetAllDevices()
        {
            List<ComputeDevice> All_Devices = new List<ComputeDevice>();

            foreach (ComputePlatform pl in ComputePlatform.Platforms)
            {
                foreach (ComputeDevice dev in pl.Devices)
                {
                    if(dev.Type == ComputeDeviceTypes.Gpu || dev.Type == ComputeDeviceTypes.Accelerator)
                    All_Devices.Add(dev);
                }
            }

            return All_Devices;
        }

        public ComputeDevice GetComputeDevice()
        {
            return Device[0];
        }


    }
}
