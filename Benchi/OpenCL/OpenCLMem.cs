﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLMem : OpenCLBenchmarkBase
    {
        ComputeBuffer<float> readBuffer;
        ComputeBuffer<float> writeBuffer;

        float limit;

        public OpenCLMem(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupsize, long workSize, TextWriter log)
        {
            globalSize = new long[1];
            localSize = new long[1];
            limit = this.globalSize[0] =  workSize*workSize;
            this.localSize = workgroupsize;
            this.log = log;
            device = new List<ComputeDevice>();
            device.Add(Device);

            context = Context;

            clProgramSource = @"__kernel void
            floatMEMBench(__global float * readBuffer,
                          __global float * writeBuffer,
                          __private float size
                        )
            {
                int xid = get_global_id(0);
                if(xid < size)
                {
                    writeBuffer[xid] = readBuffer[xid];
                }   
            }

            __kernel void
            Fill(__global float * readBuffer,
                 __global float * writeBuffer,
                 __private float size
                        )
            {
                int xid = get_global_id(0);
                if(xid < size)
                {
                     writeBuffer[xid] = 0.0f;
                     readBuffer[xid]  = 1.0f;
                }   
            }";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);

            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("floatMEMBench");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }


            kernel = program.CreateKernel("floatMEMBench");
            kernel2 = program.CreateKernel("Fill");

            commands = Commands;

            globalSize[0] = (long)((float)localSize[0] * Math.Ceiling((float)(globalSize[0] / (float)localSize[0])));

        }

        public override void Prepare()
        {

            readBuffer = new ComputeBuffer<float>(context, ComputeMemoryFlags.None, this.globalSize[0]);
            writeBuffer = new ComputeBuffer<float>(context, ComputeMemoryFlags.None, this.globalSize[0]);

            kernel.SetMemoryArgument(0, readBuffer);
            kernel.SetMemoryArgument(1, writeBuffer);
            kernel.SetValueArgument<float>(2, limit);

            kernel2.SetMemoryArgument(0, readBuffer);
            kernel2.SetMemoryArgument(1, writeBuffer);
            kernel2.SetValueArgument<float>(2, limit);
        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);
            commands.Finish();
        }

        public override void Unprepare()
        {
            readBuffer.Dispose(); 
            writeBuffer.Dispose();
        }
    }
}
