﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLCache : OpenCLBenchmarkBase
    {
        ComputeBuffer<float> m1;
        ComputeBuffer<float> m2;
        ComputeBuffer<float> result;

        int size_x = 0;
        int size_y = 0;

        public OpenCLCache(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupSize, long workSizeX, long workSizeY, TextWriter log)
        {
            this.context = Context;
            globalSize = new long[] { workSizeX, workSizeY };
            localSize = workgroupSize;
            size_x = (int)workSizeX;
            size_y = (int)workSizeY;
            device = new List<ComputeDevice>();
            device.Add(Device);
            this.log = log;

            clProgramSource = @"
            __kernel void Fill(
                            __global float * M1,
                            __global float * M2,
                            __private int size_x,
                            __private int size_y
                            )
            {
                int gidx = get_global_id(0);
                int gidy = get_global_id(1);
                int gid =  gidx + gidy*get_global_size(0); 
                if(gidx < size_x && gidy < size_y)
                {
                    M1[gid] = gid;
                    M2[gid] = gid;
                }

            }


            __kernel void
            floatCACHEBench(__global float * MResp,
                            __global float * M1,
                            __global float * M2,
                            __private int size_x,
                            __private int size_y)
            {
                // Vector element index
                int gidx = get_global_id(0);
                int gidy = get_global_id(1);

                int gid =  gidx + gidy*get_global_size(0); 

                MResp[gid] = 0;
                float endval = 0;

                if(gidx < size_x && gidy < size_y)
                {
                    for (int run = 0; run < size_x; run++)
                    {
                        endval += M1[run + gidy*size_x] * M2[gidx +  size_y*run];
                    }
                    MResp[gid] = endval;
                }
            }
            ";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);

            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("floatCACHEBench");
                kernel2 = program.CreateKernel("Fill");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }

            commands = Commands;


            globalSize[0] = (long)((float)localSize[0] * Math.Ceiling((float)(globalSize[0] / (float)localSize[0])));
            globalSize[1] = (long)((float)localSize[1] * Math.Ceiling((float)(globalSize[1] / (float)localSize[1])));
        }

        public override void Prepare()
        {
            result = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, globalSize[0] * globalSize[1]);
            m1 = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly, globalSize[0] * globalSize[1]);
            m2 = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly, globalSize[0] * globalSize[1]);

            int par_cnt = 0;
            kernel.SetMemoryArgument(par_cnt++, result);
            kernel.SetMemoryArgument(par_cnt++, m1);
            kernel.SetMemoryArgument(par_cnt++, m2);
            kernel.SetValueArgument<int>(par_cnt++, size_x);
            kernel.SetValueArgument<int>(par_cnt++, size_y);

            par_cnt = 0;
            kernel2.SetMemoryArgument(par_cnt++, m1);
            kernel2.SetMemoryArgument(par_cnt++, m2);
            kernel2.SetValueArgument<int>(par_cnt++, size_x);
            kernel2.SetValueArgument<int>(par_cnt++, size_y);
        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);
            commands.Finish();
        }

        public override void Unprepare()
        {
            m1.Dispose();
            m2.Dispose();
            result.Dispose();
        }
    }
}
