﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloo;
using System.IO;

namespace Benchi.OpenCL
{
    interface OpenCLBenchmarkInterface
    {
        void Prepare();
        void Unprepare();
        void Run();
        ComputeCommandQueue GetCommandQueue(ComputeContext context, ComputeDevice device, ComputeCommandQueueFlags properties);
    }
}
