﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLArithmetic : OpenCLBenchmarkBase
    {
        float[] parameter;
        ComputeBuffer<float> result;

        public OpenCLArithmetic(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupsize, long workSizeX, long workSizeY, TextWriter log, float[] pars)
        {
            this.log = log;
            context = Context;
            device = new List<ComputeDevice>();
            device.Add(Device);
            parameter = new float[pars.Length];
            parameter[0] = pars[0];
            parameter[1] = pars[1];
            parameter[2] = pars[2];
            parameter[3] = pars[3];
            clProgramSource = @"__kernel void
                floatCPUBench(
                            __global float * res,
                            __private float par0,
                            __private float par1,
                            __private float par2,
                            __private float par3
                            )
                {   
                float IntVal = 2;
                float FloatVal = 2.0f;        

                IntVal =  (((IntVal-par0)*par1)+par2)/par3;
                FloatVal =  (((FloatVal-par0)*par1)+par2)/par3;
                FloatVal = exp(log(FloatVal));
                res[get_global_id(0)+get_global_size(0)*get_global_id(1)] =  FloatVal - IntVal;

                }";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);

            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("floatCPUBench");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }


            commands = Commands;

            globalSize = new long[2];
            localSize = new long[2];

            //Workgroup-Size
            globalSize[0] = workSizeX;
            globalSize[1] = workSizeY;

            localSize[0] = workgroupsize[0];
            localSize[1] = workgroupsize[1];

            globalSize[0] = (long)((float)localSize[0] * Math.Ceiling((float)(globalSize[0] / (float)localSize[0])));
            globalSize[1] = (long)((float)localSize[1] * Math.Ceiling((float)(globalSize[1] / (float)localSize[1])));
        }

        public override void Prepare()
        {
            //Reset this Benchmark for re-run
            result = new ComputeBuffer<float>(this.context, ComputeMemoryFlags.None, globalSize[0] * globalSize[1]);

            int par_cnt = 0;
            kernel.SetMemoryArgument(par_cnt++, result);
            kernel.SetValueArgument<float>(par_cnt++, this.parameter[0]);
            kernel.SetValueArgument<float>(par_cnt++, this.parameter[1]);
            kernel.SetValueArgument<float>(par_cnt++, this.parameter[2]);
            kernel.SetValueArgument<float>(par_cnt++, this.parameter[3]);
        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);
            commands.Finish();
        }
        
        public override void Unprepare()
        {
            result.Dispose();
        }
    }
}
