﻿using System;
using System.Collections.Generic;
using Cloo;
using System.IO;
/*using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Benchi.OpenCL;
using System.Threading;
using Cloo.Bindings;*/

namespace Benchi.OpenCL
{
    class OpenCLMemDP : OpenCLBenchmarkBase
    {
        ComputeBuffer<double> readBuffer;
        ComputeBuffer<double> writeBuffer;

        double limit;

        public OpenCLMemDP(ComputeContext Context, ComputeDevice Device, ComputeCommandQueue Commands, long[] workgroupsize, long workSize, TextWriter log)
        {
            globalSize = new long[1];
            localSize = new long[1];
            limit = this.globalSize[0] =  workSize*workSize;
            this.localSize = workgroupsize;
            this.log = log;
            device = new List<ComputeDevice>();
            device.Add(Device);

            context = Context;

            clProgramSource = @"__kernel void
            floatMEMBench(__global double * readBuffer,
                          __global double * writeBuffer,
                          __private double size
                        )
            {
                int xid = get_global_id(0);
                if(xid < size)
                {
                    writeBuffer[xid] = readBuffer[xid];
                }   
            }

            __kernel void
            Fill(__global double * readBuffer,
                 __global double * writeBuffer,
                 __private double size
                        )
            {
                int xid = get_global_id(0);
                if(xid < size)
                {
                     writeBuffer[xid] = 0.0f;
                     readBuffer[xid]  = 1.0f;
                }   
            }";

            program = new ComputeProgram(context, clProgramSource);
            program.Build(device, null, notify, IntPtr.Zero);

            while (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.InProgress)
            { }

            if (program.GetBuildStatus(device[0]) == ComputeProgramBuildStatus.Success)
            {
                kernel = program.CreateKernel("floatMEMBench");
            }
            else
            {
                throw new BuildProgramFailureComputeException();
            }


            kernel = program.CreateKernel("floatMEMBench");
            kernel2 = program.CreateKernel("Fill");

            commands = Commands;

            globalSize[0] = (long)((double)localSize[0] * Math.Ceiling((double)(globalSize[0] / (double)localSize[0])));

        }

        public override void Prepare()
        {
            readBuffer = new ComputeBuffer<double>(context, ComputeMemoryFlags.None, this.globalSize[0]);
            writeBuffer = new ComputeBuffer<double>(context, ComputeMemoryFlags.None, this.globalSize[0]);

            kernel.SetMemoryArgument(0, readBuffer);
            kernel.SetMemoryArgument(1, writeBuffer);
            kernel.SetValueArgument<double>(2, limit);

            kernel2.SetMemoryArgument(0, readBuffer);
            kernel2.SetMemoryArgument(1, writeBuffer);
            kernel2.SetValueArgument<double>(2, limit);
        }

        public override void Run()
        {
            commands.Execute(kernel, null, globalSize, localSize, null);
            commands.Finish();
        }

        public override void Unprepare()
        {
            readBuffer.Dispose(); 
            writeBuffer.Dispose();
        }
    }
}
